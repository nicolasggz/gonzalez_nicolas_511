﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tarea_004
{
    public class Departamentos
    {
        public Departamentos()
        {
            calcPromEmpleados = 0;
            calcPromVentas = 0;
            ventaTotal = 0;
            ventaMax = 0;
            ventaMin = 0;
        }

        public int calcPromVentas;

        public int calcPromEmpleados;

        public decimal ventaTotal;

        public decimal ventaMax;

        public decimal ventaMin;
    }
}